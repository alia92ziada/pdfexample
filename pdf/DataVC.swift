//
//  DataVC.swift
//  pdf
//
//  Created by Alia Ziada on 5/21/18.
//  Copyright © 2018 Alia Ziada. All rights reserved.
//

import UIKit

class DataVC: UIViewController {

    @IBOutlet weak var scrollView: TiledPDFScrollView!
    
    var pageNumber = 0;
    var pdf: CGPDFDocument!;
    var page: CGPDFPage!;
    var myScale: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        page = pdf.page(at: pageNumber)
        scrollView.setPDFPage(page);
        
        // Disable zooming if our pages are currently shown in landscape, for new views
        scrollView.isUserInteractionEnabled = UIApplication.shared.statusBarOrientation.isPortrait
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews(){
        if page != nil {
            restoreScale();
        }
    }
    
    func restoreScale(){
        // Called on orientation change.
        // We need to zoom out and basically reset the scrollview to look right in two-page spline view.
        let pageRect = page.getBoxRect(CGPDFBox.mediaBox)
        let yScale = view.frame.size.height / pageRect.size.height
        let xScale = view.frame.size.width / pageRect.size.width
        myScale = min(xScale, yScale)
        scrollView.bounds = view.bounds
        scrollView.zoomScale = 1.0
        scrollView.PDFScale = myScale
        scrollView.tiledPDFView.bounds = view.bounds
        scrollView.tiledPDFView.myScale = myScale
        scrollView.tiledPDFView.layer.setNeedsDisplay()
    }

}
