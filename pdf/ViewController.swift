//
//  ViewController.swift
//  pdf
//
//  Created by Alia Ziada on 5/21/18.
//  Copyright © 2018 Alia Ziada. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var pageViewer: UIPageViewController!;
    var pdfDoc: CGPDFDocument!;
    var numberOfPages: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let pdfURL:URL = Bundle.main.url(forResource: "Ecommerce API V1.2.pdf", withExtension: nil){
            let documentURL:CFURL = pdfURL as CFURL
            pdfDoc = CGPDFDocument(documentURL)
            numberOfPages = pdfDoc.numberOfPages as Int
            if numberOfPages % 2 == 1{
                numberOfPages += 1
            }
        }
        
        pageViewer = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.pageCurl, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        pageViewer.delegate = self
        let startingViewController = viewControllerAtIndex(0) as DataVC
        let viewControllers:[DataVC] = [startingViewController]
        pageViewer.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        pageViewer.dataSource = self;
        self.addChildViewController(pageViewer);
        self.view.addSubview(pageViewer.view);
        
        let pageViewRect = self.view.bounds
        pageViewer.view.frame = pageViewRect
        pageViewer.didMove(toParentViewController: self)
        
        // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
        self.view.gestureRecognizers = pageViewer.gestureRecognizers
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func viewControllerAtIndex(_ index: Int) -> DataVC{
        // Create a new view controller and pass suitable data.
        let dataVC = DataVC(nibName: "DataVC", bundle: nil);
        
        dataVC.pageNumber = index + 1
        dataVC.pdf = pdfDoc
        return dataVC;
    }
    
    func indexOfViewController(_ viewController: DataVC) -> Int
    {
        // Return the index of the given data view controller.
        // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
        return viewController.pageNumber - 1
    }

}

extension ViewController: UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        
        var index = self.indexOfViewController(viewController as! DataVC)
        if index == 0 || index == NSNotFound{
            return nil
        }
        index -= 1
        return self.viewControllerAtIndex(index)
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        
        var index = self.indexOfViewController(viewController as! DataVC)
        if index == NSNotFound{
            return nil
        }
        index += 1
        if index == numberOfPages{
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
}

extension ViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        let currentViewController = pageViewer.viewControllers?[0] as! DataVC
        
        var viewControllers:[UIViewController] = []
        let indexOfCurrentViewController = indexOfViewController(currentViewController);
        if indexOfCurrentViewController % 2 == 0{
            let nextViewController: UIViewController = self.pageViewController(pageViewController, viewControllerAfter: currentViewController)!
            viewControllers = [currentViewController, nextViewController]
        }else{
            let previousViewController: UIViewController = self.pageViewController(pageViewController, viewControllerBefore: currentViewController)!
            viewControllers = [previousViewController, currentViewController]
        }
        pageViewController.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        
        return .mid;
    }
}
